import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { FirstComponent } from './first/first.component';
import { WrapperoneComponent } from './wrapperone/wrapperone.component';
import { TabFirstComponent } from './tab-first/tab-first.component';
import { TabSecondComponent } from './tab-second/tab-second.component';
import { TabThirdComponent } from './tab-third/tab-third.component';

import { SidebarComponent } from './sidebar/sidebar.component';

import { HeaderComponent } from './header/header.component';
import { QuotationListComponent } from './quotation-list/quotation-list.component';
import { LoginComponent } from './login/login.component';

import { UserService } from './services/user.service';

import { RoutingModule } from './app-routing.module';
import { FormService } from './form.service';



@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    WrapperoneComponent,
    TabFirstComponent,
    TabSecondComponent,
    TabThirdComponent,
    SidebarComponent,
    HeaderComponent,
    QuotationListComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RoutingModule,
    ReactiveFormsModule
  ],
  providers: [UserService, FormService],
  bootstrap: [AppComponent]
})
export class AppModule { }
