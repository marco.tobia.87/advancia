import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private userSrv: UserService) { }

  ngOnInit() {}

  doLogout() {
    console.log('Addio');
    this.userSrv.logout();

    //this.router.navigate(['/']);
  }
}
